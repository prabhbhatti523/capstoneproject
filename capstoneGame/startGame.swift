//
//  startGame.swift
//  capstoneGame
//
//  Created by Prabhjinder Singh on 2019-12-03.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class startGame: SKScene {
//    let startButton
//        = SKSpriteNode(imageNamed: "startButton")
    override init(size: CGSize) {
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func didMove(to view: SKView) {
        // SPRITE SETUP
        let background = SKSpriteNode(imageNamed:"bg")
        // put backgound in the middle of screen
        background.position = CGPoint(x:size.width/2, y:size.height/2)
        addChild(background)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        // when person touches screen, show the main screen
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    // let location = (touches.first as! UITouch).location(in: self.view)
     // stone tapped
           guard let mousePosition = touches.first?.location(in: self) else {
                 return
             }
                    let mainGameScreen = GameScene(size:self.size)
                    self.view?.presentScene(mainGameScreen)
//        if self.startButton.contains(mousePosition){
//            let mainGameScreen = GameScene(size:self.size)
//            self.view?.presentScene(mainGameScreen)
//        }
    
    }
}

